import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import miners from './miners.json';
import './index.css';

const Dashboard = () => {
  const [modalState, setModalState] = useState({
    open: false,
    freq: 0,
    port: 0,
    pdu: 0,
    s: 0,
    tB: 0,
    TH5s: 0,
    THAvg: 0,
    w: 0
  });

  const openModal = (miner) => {
    let freq = miner.freq;
    let port = miner.port;
    let pdu = miner.pdu;
    let s = miner.s;
    let tB = miner.tB;
    let TH5s = miner.TH5s;
    let THAvg = miner.THAvg;
    let w = miner.w;

    setModalState({ open: true, freq, port, pdu, s, tB, TH5s, THAvg, w });
  };

  const closeModal = () => {
    setModalState({ open: false, freq: 0, port: 0, pdu: 0, s: 0, tB: 0, TH5s: 0, THAvg: 0, w: 0 });
  };

  function SetTitle() {
    useEffect(() => {
      document.title = 'Miner Dashboard';
    }, []);
  }  

  function getStatus(status) {
    if (status === 10) return "OK";
    if (status === 20) return "Loss of hashrate";
    if (status === 30) return "Warning";
    if (status === 40) return "Minor problems";
    if (status === 50) return "Major problems";
    if (status === 60) return "Critical";

    return "Unknown";
  }

  function check(value) {
    return value ? value : "n/a";
  }

  function addMiner(miner) {
    let classes = "miner bg" + miner.s;
    let cell = <div className={classes} onClick={() => openModal(miner)}>{miner.port}</div>;

    return cell;
  }

  function renderMiners() {
    let pdus = [];
    let ports = [];
    let currentPDU = 1;

    miners[19].values.forEach(miner => {
      if (miner.pdu !== currentPDU) {
        currentPDU++;
        pdus.push(<div className="pdu">{ports}</div>);
        ports = [];

        if (miner.s) { ports.push(addMiner(miner)); }
      } else {
        if (miner.s) { ports.push(addMiner(miner)); }
      }
    });

    pdus.push(<div className="pdu">{ports}</div>);

    return pdus;
  }

  return (
    <div className="pad">
      <h1>Pad 18 Container 22</h1>
      {SetTitle()}
      {renderMiners()}

      {modalState.open && (
        <div className="modal">
          <div className="modal-background" onClick={closeModal}></div>
          <div className="modal-content">
            <span className="modal-close" onClick={closeModal}>&#10006;</span>
            <h2>PDU {modalState.pdu} Port {modalState.port}</h2>
            <table>
              <tbody>
                <tr>
                  <td>Status:</td>
                  <td>{getStatus(modalState.s)}</td>
                </tr>
                <tr>
                  <td>Hashrate (5s):</td>
                  <td>{check(modalState.TH5s)}</td>
                </tr>
                <tr>
                  <td>Average hashrate (1h):</td>
                  <td>{check(modalState.THAvg)}</td>
                </tr>
                <tr>
                  <td>Frequency:</td>
                  <td>{check(modalState.freq)}</td>
                </tr>
                <tr>
                  <td>Board temperature:</td>
                  <td>{check(modalState.tB)}</td>
                </tr>
                <tr>
                  <td>Power:</td>
                  <td>{check(modalState.w)}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      )}
    </div>
  );
};

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(<Dashboard />);